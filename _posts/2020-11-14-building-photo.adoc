= The best picture I ever took
:page-excerpt: Notes on a photo I took a while ago.

Out of all pictures I ever took, this is probably the one I like the most, as
well as the one that took me most effort to get right. Here it is:

image::building.png[Rundown building]

It depicts a rundown building in the center of the city I live in. Here are
some technical details:

Aperture:: f/11.0
Exposure:: 179s
Focal Length:: 25.0 (35mm film), 17.0mm (lens)
ISO:: 100

As you can see, this is a long-exposure shot. It took me quite a while to get
this to work. First of all, I needed a 10-step filter so that I could increase
the exposure time without blowing the highlights. Those things are expansive. I
highly recommend http://www.leefilters.com/[Lee Filters'] products if you're
looking for a step filter.

The weather didn't help too. For this shot to work I needed some cloud
coverage: just the right amount so that I could get the white clouds flowing
through the dark background. Not too much clouds, not too little clouds. It
took me about six attempts to get the weather I was hoping for.

I think the effort payed off. I quite like this picture and I'm very proud of
it. Here's a _making-of_ of sorts:

video::building.mp4[options="autoplay,loop"]

You can check out some other pictures of mine at https://photos.escobar.life.
