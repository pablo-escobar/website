// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

function vimKeyBind(e) {
  switch (e.key) {
    // Scroll to the top by 100px
    case "k":
    case "K":
      window.scrollBy(0, -100);
      break;
    // Scroll to the bottom by 100px
    case "j":
    case "J":
      window.scrollBy(0, 100);
      break;
    // Scroll to the top of the page
    case "g":
      window.scrollTo(0, 0);
      break;
    // Scroll to the bottom of the page
    case "G":
      window.scrollTo(0, document.body.scrollHeight);
      break;
  }
}

// Register the event handler
window.addEventListener("keydown", vimKeyBind);

