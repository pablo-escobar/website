.PHONY: install runserver build

install:
	bundle install

runserver:
	# TZ=-0000 is used to hide my local timezone from jekyll-feed
	env TZ=-0000 bundle exec jekyll serve --trace

build:
	# TZ=-0000 is used to hide my local timezone from jekyll-feed
	env TZ=-0000 bundle exec jekyll build
	# Minification of CSS files
	find _site/css/ -type f \
			-name "*.css" \
			-exec echo {} \; \
			-exec uglifycss --cute-comments --output {} {} \;

deploy: build
	rsync -rtv _site/ root@pablo.escobar.life:/var/www/pablo
